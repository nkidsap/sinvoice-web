FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-bionic
COPY bin/Release/netcoreapp3.1/Publish/ App/
WORKDIR /App
ENTRYPOINT ["dotnet", "SINVOICE_WEB.dll"]