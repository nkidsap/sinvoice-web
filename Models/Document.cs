﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Models
{
    public class Document
    {
        [Key]
        public int Docentry { get; set; }
        public string InvoiceNo { get; set; }
        public string TemplateCode { get; set; }
        public string Serial { get; set; }
        public DateTime IssueDate { get; set; }
    }
}
