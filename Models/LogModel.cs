﻿using System;
using System.Collections.Generic;

namespace SINVOICE_WEB.Models
{
    public class LogModel
    {
        public string docentry { get; set; }
        public DateTime? syncdate { get; set; }
        public string errorCode { get; set; }
        public string description { get; set; }
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        public string originalinvoiceNo { get; set; }
        public string transactionID { get; set; }
        public string reservationCode { get; set; }
        public string objectjson { get; set; }
        public string transtype { get; set; }
        public string templatecode { get; set; }
        public string serial { get; set; }
        public string issuedate_unixtime { get; set; }
        public DateTime? issueDate { get; set; }
        public bool? iscanceled { get; set; } = false;
        public bool? isupdatetax { get; set; } = false;
    }
    public class ResultDoc
    {

        public string errorCode { get; set; }

        public string description { get; set; }

        public Result result { get; set; } = new Result();

    }
    public class Result
    {
        public string invoiceNo { get; set; }
        public string transactionID { get; set; }
        public string reservationCode { get; set; }
    }
    public class FindInvoiceResult
    {
        public string errorCode { get; set; }
        public string description { get; set; }

        public int totalRow { get; set; }
        public bool? result { get; set; }
        public List<inv> invoices { get; set; } = new List<inv>();
    }
    public class inv
    {
        public string invoiceId { get; set; }
        public string invoiceType { get; set; }
        public string adjustmentType { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeri { get; set; }
        public string invoiceNumber { get; set; }
        public string invoiceNo { get; set; }
        public string currency { get; set; }
        public string total { get; set; }
        public long issueDate { get; set; }
        public string issueDateStr { get; set; }
        public string state { get; set; }
        public long? requestDate { get; set; }
        public string description { get; set; }
        public string buyerIdNo { get; set; }
        public string stateCode { get; set; }
        public string subscriberNumber { get; set; }
        public string buyerCode { get; set; }
        public string paymentStatus { get; set; }
        public string viewStatus { get; set; }
        public string downloadStatus { get; set; }
        public string exchangeStatus { get; set; }
        public string numOfExchange { get; set; }
        public long? createTime { get; set; }
        public string contractId { get; set; }
        public string contractNo { get; set; }
        public string supplierTaxCode { get; set; }
        public string buyerTaxCode { get; set; }
        public string totalBeforeTax { get; set; }
        public string taxAmount { get; set; }
        public string taxRate { get; set; }
        public string paymentMethod { get; set; }
        public long? paymentTime { get; set; }
        public string customerId { get; set; }
        public string buyerName { get; set; }
        public string no { get; set; }
        public string paymentStatusName { get; set; }
    }
    public class FindInvoiceSeriResult
    {
        public string errorCode { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public int? numOfpublishInv { get; set; }
        public int? totalInv { get; set; }
    }
    public class FindInvoice
    {
        
        public string supplierTaxCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string invoiceType { get; set; }
        public int rowPerPage { get; set; }
        public int pageNum { get; set; }
        public string templateCode { get; set; }
        //public bool getAll { get; set; } = false;
    }
    public class FindInvoiceSeri
    {

        public string supplierTaxCode { get; set; } 

        public string templateCode { get; set; } 
        public string serial { get; set; } 
    }
    public class Invoice
    {
        public string invoiceId { get; set; }
        public string invoiceType { get; set; }
        public string adjustmentType { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeri { get; set; }
        public string invoiceNumber { get; set; }
        public string invoiceNo { get; set; }
        public string currency { get; set; }
        public string total { get; set; }
        public long issueDate { get; set; }
        public string issueDateStr { get; set; }
        public string state { get; set; }
        public long? requestDate { get; set; }
        public string description { get; set; }
        public string buyerIdNo { get; set; }
        public string stateCode { get; set; }
        public string subscriberNumber { get; set; }
        public string buyerCode { get; set; }
        public string paymentStatus { get; set; }
        public string viewStatus { get; set; }
        public string downloadStatus { get; set; }
        public string exchangeStatus { get; set; }
        public string numOfExchange { get; set; }
        public long? createTime { get; set; }
        public string contractId { get; set; }
        public string contractNo { get; set; }
        public string supplierTaxCode { get; set; }
        public string buyerTaxCode { get; set; }
        public string totalBeforeTax { get; set; }
        public string taxAmount { get; set; }
        public string taxRate { get; set; }
        public string paymentMethod { get; set; }
        public long? paymentTime { get; set; }
        public string customerId { get; set; }
        public string buyerName { get; set; }
        public string no { get; set; }
        public string paymentStatusName { get; set; }
    }
    public class ResultFile
    {

        public string errorCode { get; set; }

        public string description { get; set; }

        public string fileName { get; set; }

        public string fileToBytes { get; set; }
        public bool? paymentStatus { get; set; }

    }
}