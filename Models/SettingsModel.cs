﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Models
{
    public class SettingsModel
    {
        public string ApiUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class SmtpSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class Mail
    {
        public string To { get; set; }
        public string CC { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
