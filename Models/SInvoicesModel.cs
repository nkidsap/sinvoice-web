﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Models
{
    public class SInvoicesModel
    {
        public sellerInfo sellerInfo { get; set; } = new sellerInfo();
        public buyerInfo buyerInfo { get; set; } = new buyerInfo();

        public generalInvoiceInfo generalInvoiceInfo { get; set; } = new generalInvoiceInfo();
        public deliveryInfo deliveryInfo { get; set; } = new deliveryInfo();
        public List<itemInfo> itemInfo { get; set; } = new List<itemInfo>();
        public List<payments> payments { get; set; } = new List<payments>();
        public List<taxBreakdowns> taxBreakdowns { get; set; } = new List<taxBreakdowns>();
        public summarizeInfo summarizeInfo { get; set; } = new summarizeInfo();
        public List<metadata> metadata { get; set; } = new List<metadata>();
    }
    public class buyerInfo
    {

        public string buyerName { get; set; }

        public string buyerCode { get; set; }
        public string buyerLegalName { get; set; }

        public string buyerTaxCode { get; set; }

        public string buyerAddressLine { get; set; }
        public string buyerPhoneNumber { get; set; }

        public string buyerFaxNumber { get; set; }
        public string buyerEmail { get; set; }
        public string buyerBankName { get; set; }
        public string buyerBankAccount { get; set; }
        public string buyerDistrictName { get; set; }

        public string buyerCityName { get; set; }
        public string buyerCountryCode { get; set; }

        public string buyerIdType { get; set; }

        public string buyerIdNo { get; set; }

        public DateTime? buyerBirthDay { get; set; }
    }
    public class itemInfo
    {
        public int? lineNumber { get; set; }
        public int selection { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public decimal? unitPrice { get; set; }
        public decimal? quantity { get; set; }
        public decimal? itemTotalAmountWithoutTax { get; set; }

        public decimal? taxPercentage { get; set; }
        public decimal? taxAmount { get; set; }
        public decimal? adjustmentTaxAmount { get; set; } = 0;
        public bool? isIncreaseItem { get; set; }
        public string itemNote { get; set; }
        public string batchNo { get; set; }
        public string expDate { get; set; }
        public decimal? discount { get; set; }
        public decimal? discount2 { get; set; }
        public decimal? itemDiscount { get; set; }
        public decimal? itemTotalAmountAfterDiscount { get; set; }
        public decimal? itemTotalAmountWithTax { get; set; }
    }
    public class taxBreakdowns
    {
        public decimal? taxPercentage { get; set; }
        public decimal? taxableAmount { get; set; }
        public decimal? taxAmount { get; set; }
        public decimal? taxableAmountPos { get; set; }
        public decimal? taxAmountPos { get; set; }
        public string taxExemptionReason { get; set; }
    }
    public class summarizeInfo
    {
        public decimal? sumOfTotalLineAmountWithoutTax { get; set; }
        public decimal? totalAmountWithoutTax { get; set; }
        public decimal? totalTaxAmount { get; set; }
        public decimal? totalAmountWithTax { get; set; }
        public decimal? totalAmountWithTaxFrn { get; set; }
        public decimal? discountAmount { get; set; }

        public string totalAmountWithTaxInWords { get; set; }

        public bool isTotalAmountPos { get; set; }
        public bool isTotalTaxAmountPos { get; set; }
        public bool isTotalAmtWithoutTaxPos { get; set; }
        public decimal? totalAmountAfterDiscount { get; set; }
        public decimal? settlementDiscountAmount { get; set; }
        public bool isDiscountAmtPos { get; set; }
    }
    public class metadata
    {
        public string keyTag { get; set; }
        public string valueType { get; set; }
        public DateTime? dateValue { get; set; }
        public string stringValue { get; set; }
        public int numberValue { get; set; }
        public string keyLabel { get; set; }
        public bool isRequired { get; set; }
        public bool isSeller { get; set; }
    }
    public class generalInvoiceInfo
    {
        public string invoiceType { get; set; }
        public string invoiceNo { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeries { get; set; }
        public DateTime? invoiceIssuedDate { get; set; }
        public string currencyCode { get; set; }
        public string adjustmentType { get; set; }
        public string adjustmentInvoiceType { get; set; }
        public string originalInvoiceId { get; set; }


        public long originalInvoiceIssueDate { get; set; }

        public string additionalReferenceDesc { get; set; }

        public long additionalReferenceDate { get; set; }

        public bool paymentStatus { get; set; } = true;
        public bool cusGetInvoiceRight { get; set; } = true;

        public decimal? exchangeRate { get; set; }
        public string transactionUuid { get; set; }
        public string userName { get; set; }
        public string certificateSerial { get; set; }

    }
    public class deliveryInfo
    {
        public string deliveryOrderNumber { get; set; }
        public DateTime? deliveryOrderDate { get; set; }
        public string deliveryOrderBy { get; set; }
        public string deliveryBy { get; set; }
        public string fromWarehouseName { get; set; }
        public string toWarehouseName { get; set; }
        public string transportationMethod { get; set; }
        public string containerNumber { get; set; }
        public string deliveryOrderContent { get; set; }
    }
    public class payments
    {
        public string paymentMethodName { get; set; }
    }
    public class sellerInfo
    {
        //Tên (đăng ký kinh doanh trong trường hợp là doanh nghiệp) của người bán
        [Required]
        public string sellerLegalName { get; set; }
        //Mã số thuế người bán được cấp bởi TCT Việt Nam. 
        [Required]
        public string sellerTaxCode { get; set; }
        public string sellerCode { get; set; }
        //Địa chỉ của bên bán được thể hiện trên hóa đơn.
        [Required]
        public string sellerAddressLine { get; set; }

        public string sellerPhoneNumber { get; set; }
        public string sellerFaxNumber { get; set; }

        public string sellerEmail { get; set; }
        public string sellerBankName { get; set; }
        public string sellerBankAccount { get; set; }
        public string sellerDistrictName { get; set; }
        public string sellerCityName { get; set; }

        public string sellerCountryCode { get; set; }
        public string sellerWebsite { get; set; }
    }
}
