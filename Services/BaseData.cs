﻿using Dapper;
using SINVOICE_BASE;
using SINVOICE_WEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Services
{
    public class BaseData
    {
        private static string connectstring = "";

        public static void SetDBConnection(string _connectstring)
        {
            connectstring = _connectstring;
        }
        public static async Task<invoice> GetInvoiceInfo(string DocEntry, TransType TransType, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_INVOICE_INFO",
                    new { @DocEntry = DocEntry, @TransType = TransType },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                SINVOICE_BASE.sellerInfo sel = result.Read<SINVOICE_BASE.sellerInfo>().FirstOrDefault();

                SINVOICE_BASE.generalInvoiceInfo gen = result.Read<SINVOICE_BASE.generalInvoiceInfo>().FirstOrDefault();

                SINVOICE_BASE.buyerInfo buy = result.Read<SINVOICE_BASE.buyerInfo>().FirstOrDefault();
                List<SINVOICE_BASE.payments> pms = result.Read<SINVOICE_BASE.payments>().ToList();
                SINVOICE_BASE.summarizeInfo sum = result.Read<SINVOICE_BASE.summarizeInfo>().FirstOrDefault();

                List<SINVOICE_BASE.taxBreakdowns> taxs = result.Read<SINVOICE_BASE.taxBreakdowns>().ToList();
                List<SINVOICE_BASE.itemInfo> its = result.Read<SINVOICE_BASE.itemInfo>().ToList();

                //item info discount header
                if (TransType != TransType.Transfer)
                {
                    try
                    {
                        List<SINVOICE_BASE.itemInfo> its_discount = result.Read<SINVOICE_BASE.itemInfo>().ToList();
                        foreach (SINVOICE_BASE.itemInfo it in its_discount)
                        {
                            its.Add(it);
                        }
                    }
                    catch { }

                }
                SINVOICE_BASE.deliveryInfo deli = new SINVOICE_BASE.deliveryInfo();
                if (TransType == TransType.Transfer)
                {
                    try
                    {
                        deli = result.Read<SINVOICE_BASE.deliveryInfo>().FirstOrDefault();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum,
                    deliveryInfo = deli
                };


                return inv;



            }

        }

        public static async Task<invoice> GetARCreditInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                SINVOICE_BASE.sellerInfo sel = result.Read<SINVOICE_BASE.sellerInfo>().FirstOrDefault();

                SINVOICE_BASE.generalInvoiceInfo gen = result.Read<SINVOICE_BASE.generalInvoiceInfo>().FirstOrDefault();

                SINVOICE_BASE.buyerInfo buy = result.Read<SINVOICE_BASE.buyerInfo>().FirstOrDefault();
                List<SINVOICE_BASE.payments> pms = result.Read<SINVOICE_BASE.payments>().ToList();
                SINVOICE_BASE.summarizeInfo sum = result.Read<SINVOICE_BASE.summarizeInfo>().FirstOrDefault();

                List<SINVOICE_BASE.taxBreakdowns> taxs = result.Read<SINVOICE_BASE.taxBreakdowns>().ToList();
                List<SINVOICE_BASE.itemInfo> its = result.Read<SINVOICE_BASE.itemInfo>().ToList();

                //item info discount header
                List<SINVOICE_BASE.itemInfo> its_discount = result.Read<SINVOICE_BASE.itemInfo>().ToList();
                foreach (SINVOICE_BASE.itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<invoice> GetARCreditAdjustInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_ADJUST_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                SINVOICE_BASE.sellerInfo sel = result.Read<SINVOICE_BASE.sellerInfo>().FirstOrDefault();

                SINVOICE_BASE.generalInvoiceInfo gen = result.Read<SINVOICE_BASE.generalInvoiceInfo>().FirstOrDefault();

                SINVOICE_BASE.buyerInfo buy = result.Read<SINVOICE_BASE.buyerInfo>().FirstOrDefault();
                List<SINVOICE_BASE.payments> pms = result.Read<SINVOICE_BASE.payments>().ToList();
                SINVOICE_BASE.summarizeInfo sum = result.Read<SINVOICE_BASE.summarizeInfo>().FirstOrDefault();

                List<SINVOICE_BASE.taxBreakdowns> taxs = result.Read<SINVOICE_BASE.taxBreakdowns>().ToList();
                List<SINVOICE_BASE.itemInfo> its = result.Read<SINVOICE_BASE.itemInfo>().ToList();

                //item info discount header
                List<SINVOICE_BASE.itemInfo> its_discount = result.Read<SINVOICE_BASE.itemInfo>().ToList();
                foreach (SINVOICE_BASE.itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<invoice> GetAdjustInvoiceInfo(string DocEntry, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_ORIN_INFO",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                SINVOICE_BASE.sellerInfo sel = result.Read<SINVOICE_BASE.sellerInfo>().FirstOrDefault();

                SINVOICE_BASE.generalInvoiceInfo gen = result.Read<SINVOICE_BASE.generalInvoiceInfo>().FirstOrDefault();

                SINVOICE_BASE.buyerInfo buy = result.Read<SINVOICE_BASE.buyerInfo>().FirstOrDefault();
                List<SINVOICE_BASE.payments> pms = result.Read<SINVOICE_BASE.payments>().ToList();
                SINVOICE_BASE.summarizeInfo sum = result.Read<SINVOICE_BASE.summarizeInfo>().FirstOrDefault();

                List<SINVOICE_BASE.taxBreakdowns> taxs = result.Read<SINVOICE_BASE.taxBreakdowns>().ToList();
                List<SINVOICE_BASE.itemInfo> its = result.Read<SINVOICE_BASE.itemInfo>().ToList();

                //item info discount header
                List<SINVOICE_BASE.itemInfo> its_adjust = result.Read<SINVOICE_BASE.itemInfo>().ToList();
                foreach (SINVOICE_BASE.itemInfo it in its_adjust)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }
        public static async Task<sinvoicelog> GetSInvoiceCancelInfo(string DocEntry, TransType _TransType)
        {
            //string _type = "";
            //switch (_TransType)
            //{
            //    case TransType.ARInvoice:
            //        _type = "OINV";
            //        break;
            //    case TransType.ARCreditMemo:
            //        _type = "ORIN";
            //        break;
            //    case TransType.Transfer:
            //        _type = "OWTR";
            //        break;
            //    default:
            //        break;
            //}

            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                       "USP_SINVOICE_GET_CANCEL_DOC",
                       new { @DocEntry = DocEntry, @TransType = _TransType },
                       null, Int32.MaxValue, CommandType.StoredProcedure);
                sinvoicelog log = result.Read<sinvoicelog>().FirstOrDefault();
                return log;
            }

        }
        public static async Task<sinvoicelog> GetSInvoiceInfo(string DocEntry, TransType _TransType)
        {
            string _type = "";
            switch (_TransType)
            {
                case TransType.ARInvoice:
                    _type = "OINV";
                    break;
                case TransType.ARCreditMemo:
                    _type = "ORIN";
                    break;
                case TransType.Transfer:
                    _type = "OWTR";
                    break;
                default:
                    break;
            }

            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                        string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where DocEntry={0} and transtype='{1}'  and invoiceNo is not null", DocEntry, _type),
                        null,
                        null, Int32.MaxValue, CommandType.Text);
                sinvoicelog log = result.Read<sinvoicelog>().FirstOrDefault();
                return log;
            }

        }
        public static async Task<AdditionalReference> GetSInvoiceAdditionalReference(string InvoiceNo)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_ADDITIONALREFERENCE",
                    new { @INVOICENO = InvoiceNo },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                AdditionalReference log = result.Read<AdditionalReference>().FirstOrDefault();


                return log;



            }

        }
        public static async Task<sinvoicelog> GetSInvoiceInfoByInvNo(string InvoiceNo)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where invoiceNo='{0}'  and invoiceNo is not null", InvoiceNo),
                    null,
                    null, Int32.MaxValue, CommandType.Text);

                sinvoicelog log = result.Read<sinvoicelog>().FirstOrDefault();


                return log;



            }

        }
        public static async Task SaveSinvoiceLog(sinvoicelog log, string action)
        {
            try
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @action = action, @docentry = log.docentry, @errorCode = log.errorCode, @description = log.description, @supplierTaxCode = log.supplierTaxCode, @invoiceNo = log.invoiceNo, @transactionID = log.transactionID, @reservationCode = log.reservationCode, @objectjson = log.objectjson, @transtype = log.transtype, @originalinvoiceNo = log.originalinvoiceNo,
                            @templatecode = log.templatecode, @serial = log.serial, @isupdatetax = log.isupdatetax, @iscanceled = log.iscanceled, @issuedate_unixtime = log.issuedate_unixtime, @issueDate = log.issueDate, @paymenttype = log.paymenttype, @ispayment = log.ispayment,
                            @transactionUuid = log.transactionUuid },
                        null, Int32.MaxValue, CommandType.StoredProcedure);

                }
            }
            catch (Exception ex)
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @action = action, @docentry = log.docentry, @errorCode = "444", @description = ex.Message.ToString(), @supplierTaxCode = log.supplierTaxCode, @invoiceNo = "", @transactionID = "", @reservationCode = "", @objectjson = log.objectjson, @transtype = log.transtype, @paymenttype = log.paymenttype, @ispayment = log.ispayment },
                        null, Int32.MaxValue, CommandType.StoredProcedure);
                }

            }

        }

        public static async Task<List<DocPublish>> GetListDocPublish(DateTime FromDate, DateTime ToDate)
        {
            try
            {

                // Act
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.QueryMultipleAsync(
                        "USP_SINVOICE_GET_DOC",
                        new { @FromDate = FromDate, @ToDate = ToDate },
                        null, Int32.MaxValue, CommandType.StoredProcedure);

                    List<DocPublish> docs = result.Read<DocPublish>().ToList();



                    return docs;



                }

            }
            catch (Exception ex)
            {

                
                return null;
            }

        }

    }

}
