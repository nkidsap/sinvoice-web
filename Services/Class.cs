﻿using Dapper;
using SINVOICE_WEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Services
{
    public class BaseData
    {
        private static string connectstring = "";

        public static void SetDBConnection(string _connectstring)
        {
            connectstring = _connectstring;
        }

        public static async Task<SInvoicesModel> GetInvoiceInfo(string DocEntry, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_INVOICE_INFO",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                SInvoicesModel inv = new SInvoicesModel()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<SInvoicesModel> GetARCreditInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                SInvoicesModel inv = new SInvoicesModel()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<SInvoicesModel> GetARCreditAdjustInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_ADJUST_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                SInvoicesModel inv = new SInvoicesModel()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<SInvoicesModel> GetAdjustInvoiceInfo(string DocEntry, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_ORIN_INFO",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_adjust = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_adjust)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                SInvoicesModel inv = new SInvoicesModel()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<LogModel> GetSInvoiceInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where DocEntry={0} and errorCode is null and invoiceNo is not null", DocEntry),
                    null,
                    null, Int32.MaxValue, CommandType.Text);

                LogModel log = result.Read<LogModel>().FirstOrDefault();


                return log;



            }

        }

        public static async Task<LogModel> GetSInvoiceInfoByInvNo(string InvoiceNo)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where invoiceNo='{0}' and errorCode is null and invoiceNo is not null", InvoiceNo),
                    null,
                    null, Int32.MaxValue, CommandType.Text);

                LogModel log = result.Read<LogModel>().FirstOrDefault();


                return log;



            }

        }
        public static async Task SaveSinvoiceLog(LogModel log)
        {
            try
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @docentry = log.docentry, @errorCode = log.errorCode, @description = log.description, @supplierTaxCode = log.supplierTaxCode, @invoiceNo = log.invoiceNo, @transactionID = log.transactionID, @reservationCode = log.reservationCode, @objectjson = log.objectjson, @transtype = log.transtype, @originalinvoiceNo = log.originalinvoiceNo, @templatecode = log.templatecode, @serial = log.serial, @isupdatetax = log.isupdatetax, @iscanceled = log.iscanceled, @issuedate_unixtime = log.issuedate_unixtime, @issueDate = log.issueDate },
                        null, Int32.MaxValue, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @docentry = log.docentry, @errorCode = "444", @description = ex.Message.ToString(), @supplierTaxCode = log.supplierTaxCode, @invoiceNo = "", @transactionID = "", @reservationCode = "", @objectjson = log.objectjson, @transtype = log.transtype },
                        null, Int32.MaxValue, CommandType.StoredProcedure);
                }

            }

        }
    }

}
