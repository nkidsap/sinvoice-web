﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using SINVOICE_WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Services
{
    public interface IMailer
    {
        Task SendEmailAsync(string to, string cc, string subject, string body);
        Task SendEmailAsync(Mail m);
    }

    public class Mailer : IMailer
    {
        private readonly SmtpSettings _smtpSettings;
        private readonly IWebHostEnvironment _env;

        public Mailer(IOptions<SmtpSettings> smtpSettings, IWebHostEnvironment env)
        {
            _smtpSettings = smtpSettings.Value;
            _env = env;
        }

        public async Task SendEmailAsync(string to, string cc, string subject, string body)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderEmail));
                if (to != null && to.Length > 0)
                {
                    if (to.Contains(";"))
                    {
                        string[] lsTo = to.Split(';');
                        foreach (string _to in lsTo)
                        {
                            message.To.Add(new MailboxAddress(_to));
                        }
                    }
                    else
                        message.To.Add(new MailboxAddress(to));
                }
                if (cc != null && cc.Length > 0)
                {
                    if (cc.Contains(";"))
                    {
                        string[] lsCC = cc.Split(';');
                        foreach (string _cc in lsCC)
                        {
                            message.Cc.Add(new MailboxAddress(_cc));
                        }
                    }
                    else
                        message.Cc.Add(new MailboxAddress(cc));
                }
                message.Subject = subject;
                message.Body = new TextPart("html")
                {
                    Text = body
                };

                using (var client = new SmtpClient())
                {

                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync(_smtpSettings.Server, _smtpSettings.Port, SecureSocketOptions.StartTls);

                    await client.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }



        public async Task SendEmailAsync(Mail m)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderEmail));

                if (m.To != null && m.To.Length > 0)
                {
                    if (m.To.Contains(";"))
                    {
                        string[] lsTo = m.To.Split(';');
                        foreach (string To in lsTo)
                        {
                            message.To.Add(new MailboxAddress(To));
                        }
                    }
                    else
                        message.To.Add(new MailboxAddress(m.To));
                }


                if (m.CC != null && m.CC.Length > 0)
                {
                    if (m.CC.Contains(";"))
                    {
                        string[] lsCC = m.CC.Split(';');
                        foreach (string CC in lsCC)
                        {
                            message.Cc.Add(new MailboxAddress(CC));
                        }
                    }
                    else
                        message.Cc.Add(new MailboxAddress(m.CC));
                }

                message.Subject = m.Subject;
                message.Body = new TextPart("html")
                {
                    Text = m.Body
                };

                using (var client = new SmtpClient())
                {

                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync(_smtpSettings.Server, _smtpSettings.Port, SecureSocketOptions.StartTls);

                    await client.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }
    }
}
