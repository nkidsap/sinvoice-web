﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SINVOICE_BASE;
using SINVOICE_WEB.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using inv = SINVOICE_WEB.Models.inv;

namespace SINVOICE_WEB.Services
{
    public class EInvoice : IEInvoice
    {

        private readonly SettingsModel _settingapi;
       
        public EInvoice(IOptions<SettingsModel> settingapi)
        {
            
            _settingapi = settingapi.Value;
        }

       
        public async Task<sinvoicelog> AdjustInvoice(invoice inv, string adjustmentInvoiceType, bool isIncreaseItem = true)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceWS/createInvoice/" + inv.sellerInfo.sellerTaxCode;

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;
                if (inv.generalInvoiceInfo.originalInvoiceId != null)
                {
                    inv.generalInvoiceInfo.adjustmentType = "5";
                    inv.generalInvoiceInfo.adjustmentInvoiceType = adjustmentInvoiceType;
                    if (adjustmentInvoiceType == "1")

                        inv.itemInfo.ForEach(i => i.isIncreaseItem = isIncreaseItem);

                }


                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    ResultDoc output = JsonConvert.DeserializeObject<ResultDoc>(response.Content.ReadAsStringAsync().Result);


                    if (output.errorCode != null)
                    {
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        return log;

                        //string log = WMS.ToXmlUsingDataContract(SO);
                        //WMS.SaveWMSLog("ORDR", output.Status.ToUpper(), SO.DocNumber, "", "", log, output.Message);
                        //Helper.WriteErrorLog(SO.DocNumber + "-" + output.Message.ToString());
                    }

                    else
                    {
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        log.invoiceNo = output.result.invoiceNo;
                        log.transactionID = output.result.transactionID;
                        log.reservationCode = output.result.reservationCode;
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        return log;

                    }


                }
                else
                {
                    log.errorCode = response.IsSuccessStatusCode.ToString();
                    log.description = response.ReasonPhrase;
                    return log;
                    //string log = WMS.ToXmlUsingDataContract(SO);
                    //WMS.SaveWMSLog("ORDR", "FAILED", SO.DocNumber, "", "", log, response.IsSuccessStatusCode.ToString());
                    //Helper.WriteErrorLog(SO.DocNumber + "-" + response.IsSuccessStatusCode.ToString());
                }

            }
        }

        public async Task<sinvoicelog> CancelInvoice(string supplierTaxCode, string invoiceNo, string strIssueDate, string additionalReferenceDesc, string additionalReferenceDate)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceWS/cancelTransactionInvoice";
            //dynamic inv = new ExpandoObject();
            //inv.supplierTaxCode = supplierTaxCode;

            //inv.invoiceNo = invoiceNo;
            //inv.strIssueDate = strIssueDate;
            //inv.additionalReferenceDesc = additionalReferenceDesc;
            //inv.additionalReferenceDate = additionalReferenceDate;

            var dict = new Dictionary<string, string>();
            dict.Add("supplierTaxCode", supplierTaxCode);
            dict.Add("invoiceNo", invoiceNo);
            dict.Add("strIssueDate", strIssueDate);
            dict.Add("additionalReferenceDesc", additionalReferenceDesc);
            dict.Add("additionalReferenceDate", additionalReferenceDate);
            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;

                var content = new StringContent(JsonConvert.SerializeObject(dict, Formatting.Indented), Encoding.UTF8, "application/x-www-form-urlencoded");

                log.objectjson = JsonConvert.SerializeObject(dict, Formatting.Indented);
                //var response = client.PostAsync(api_url, new FormUrlEncodedContent(dict)).Result;
                var response = await client.PostAsync(api_url, new FormUrlEncodedContent(dict));
                if (response.IsSuccessStatusCode)
                {
                    FindInvoiceResult output = JsonConvert.DeserializeObject<FindInvoiceResult>(response.Content.ReadAsStringAsync().Result);

                    if (output.errorCode != null)
                    {
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        return log;
                    }

                    else
                    {
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        return log;
                        //FileHelper.CreateFile("pdf", output.fileName, output.fileToBytes);
                    }

                }
                else
                {
                    log.errorCode = response.IsSuccessStatusCode.ToString();
                    log.description = response.ReasonPhrase;
                    return log;
                }

            }
        }

        public async Task<sinvoicelog> CreateDraftInvoice(invoice inv)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceWS/createOrUpdateInvoiceDraft/" + inv.sellerInfo.sellerTaxCode;

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;

                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    ResultDoc output = JsonConvert.DeserializeObject<ResultDoc>(response.Content.ReadAsStringAsync().Result);

                    dynamic _output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                    if (output.errorCode != null)
                    {
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        return log;

                        //string log = WMS.ToXmlUsingDataContract(SO);
                        //WMS.SaveWMSLog("ORDR", output.Status.ToUpper(), SO.DocNumber, "", "", log, output.Message);
                        //Helper.WriteErrorLog(SO.DocNumber + "-" + output.Message.ToString());
                    }

                    else
                    {
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        //log.invoiceNo = output.result.invoiceNo;
                        //log.transactionID = output.result.transactionID;
                        //log.reservationCode = output.result.reservationCode;
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        return log;

                    }


                }
                else
                {
                    log.errorCode = response.IsSuccessStatusCode.ToString();
                    log.description = response.ReasonPhrase;
                    return log;
                    //string log = WMS.ToXmlUsingDataContract(SO);
                    //WMS.SaveWMSLog("ORDR", "FAILED", SO.DocNumber, "", "", log, response.IsSuccessStatusCode.ToString());
                    //Helper.WriteErrorLog(SO.DocNumber + "-" + response.IsSuccessStatusCode.ToString());
                }

            }
        }

        public async Task<List<inv>> FindInvoice(string supplierTaxCode, string startDate, string endDate, string invoiceSeri, int rowPerPage, int pageNum, string templateCode, string invoiceNo, bool getAll = false)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceUtilsWS/getInvoices/" + supplierTaxCode;
            dynamic inv = new ExpandoObject();
            inv.startDate = startDate + "T00:00:00.000";
            inv.endDate = endDate + DateTime.Now.ToString("THH:mm:ss.fff+07:00");
            inv.invoiceSeri = invoiceSeri;
            inv.rowPerPage = rowPerPage;
            inv.pageNum = pageNum;
            inv.templateCode = templateCode;
            inv.getAll = getAll;

            inv.invoiceNo = invoiceNo == null ? "#" : invoiceNo;
            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;

                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                //log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    FindInvoiceResult output = JsonConvert.DeserializeObject<FindInvoiceResult>(response.Content.ReadAsStringAsync().Result);

                    if (output.errorCode != null)
                    {
                        //log.errorCode = output.errorCode;
                        //log.description = output.description;
                        return new List<inv>();
                    }

                    else
                    {
                        return output.invoices;
                        //FileHelper.CreateFile("pdf", output.fileName, output.fileToBytes);
                    }

                }
                else
                {
                    return new List<inv>();
                }

            }
        }

        public async Task<ResultFile> GetPDFInvoice(string supplierTaxCode, string invoiceNo, string strIssueDate, string exchangeUser)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = string.Format("InvoiceAPI/InvoiceWS/createExchangeInvoiceFile?supplierTaxCode={0}&invoiceNo= {1}&strIssueDate={2}&exchangeUser={3}", supplierTaxCode, invoiceNo, strIssueDate, exchangeUser);

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;

                //var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                //log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                
                
                //var response = client.GetAsync(api_url).Result;
                var response = await client.GetAsync(api_url);
                if (response.IsSuccessStatusCode)
                {
                    ResultFile output = JsonConvert.DeserializeObject<ResultFile>(response.Content.ReadAsStringAsync().Result);
                    return output;
                    //if (output.errorCode != null)
                    //{
                    //    log.errorCode = output.errorCode;
                    //    log.description = output.description;

                    //}

                    //else
                    //{
                    //    FileHelper.CreateFile("pdf", output.fileName, output.fileToBytes);
                    //}

                }
                else
                {
                    return new ResultFile() { errorCode = response.IsSuccessStatusCode.ToString(), description = response.ReasonPhrase, fileName = null, fileToBytes = null };
                }

            }
        }

        public async Task<ResultFile> PreviewInvoice(invoice inv)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceUtilsWS/createInvoiceDraftPreview/" + inv.sellerInfo.sellerTaxCode;

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;

                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    ResultFile output = JsonConvert.DeserializeObject<ResultFile>(response.Content.ReadAsStringAsync().Result);
                    return output;
                    //if (output.errorCode != null)
                    //{
                    //    log.errorCode = output.errorCode;
                    //    log.description = output.description;

                    //}

                    //else
                    //{
                    //    return Convert.FromBase64String(output.fileToBytes);

                    //}

                }
                else
                {
                    return new ResultFile() { errorCode = response.IsSuccessStatusCode.ToString(), description = response.ReasonPhrase, fileName = null, fileToBytes = null };
                }

            }
        }

        public async Task<sinvoicelog> PublishInvoice(invoice inv)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceWS/createInvoice/" + inv.sellerInfo.sellerTaxCode;

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;
                inv.generalInvoiceInfo.adjustmentType = "1";
                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    ResultDoc output = JsonConvert.DeserializeObject<ResultDoc>(response.Content.ReadAsStringAsync().Result);


                    if (output.errorCode != null)
                    {
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        return log;

                        //string log = WMS.ToXmlUsingDataContract(SO);
                        //WMS.SaveWMSLog("ORDR", output.Status.ToUpper(), SO.DocNumber, "", "", log, output.Message);
                        //Helper.WriteErrorLog(SO.DocNumber + "-" + output.Message.ToString());
                    }

                    else
                    {
                        log.errorCode = output.errorCode;
                        log.description = output.description;
                        log.invoiceNo = output.result.invoiceNo;
                        log.transactionID = output.result.transactionID;
                        log.reservationCode = output.result.reservationCode;
                        log.supplierTaxCode = inv.sellerInfo.sellerTaxCode;
                        return log;

                    }


                }
                else
                {
                    log.errorCode = response.IsSuccessStatusCode.ToString();
                    log.description = response.ReasonPhrase;
                    return log;
                    //string log = WMS.ToXmlUsingDataContract(SO);
                    //WMS.SaveWMSLog("ORDR", "FAILED", SO.DocNumber, "", "", log, response.IsSuccessStatusCode.ToString());
                    //Helper.WriteErrorLog(SO.DocNumber + "-" + response.IsSuccessStatusCode.ToString());
                }

            }

        }

        public async Task<sinvoicelog> ResendMail(string supplierTaxCode, string TransactionUuid)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settingapi.Username}:{_settingapi.Password}")));
            var api_url = @"InvoiceAPI/InvoiceUtilsWS/sendHtmlMailProcess";
            dynamic inv = new ExpandoObject();
            inv.supplierTaxCode = supplierTaxCode;
            inv.lstTransactionUuid = TransactionUuid;

            using (var client = new HttpClient())
            {
                sinvoicelog log = new sinvoicelog();
                client.BaseAddress = new Uri(_settingapi.ApiUrl);
                // Add an Accept header for JSON format. 
                client.Timeout = new TimeSpan(0, 5, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = authValue;
                
                var content = new StringContent(JsonConvert.SerializeObject(inv, Formatting.Indented), Encoding.UTF8, "application/json");

                log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
                //var response = client.PostAsync(api_url, content).Result;
                var response = await client.PostAsync(api_url, content);
                if (response.IsSuccessStatusCode)
                {
                    commonOutput output = JsonConvert.DeserializeObject<commonOutput>(response.Content.ReadAsStringAsync().Result);


                    if (output.commonOutputs.FirstOrDefault<mailresult>().errorCode != null)
                    {
                        
                        log.errorCode = output.commonOutputs.FirstOrDefault<mailresult>().errorCode;
                        log.description = output.commonOutputs.FirstOrDefault<mailresult>().description;
                        return log;

                        //string log = WMS.ToXmlUsingDataContract(SO);
                        //WMS.SaveWMSLog("ORDR", output.Status.ToUpper(), SO.DocNumber, "", "", log, output.Message);
                        //Helper.WriteErrorLog(SO.DocNumber + "-" + output.Message.ToString());
                    }

                    else
                    {
                        log.errorCode = output.commonOutputs.FirstOrDefault<mailresult>().errorCode;
                        log.description = output.commonOutputs.FirstOrDefault<mailresult>().description;
                        
                        return log;

                    }


                }
                else
                {
                    log.errorCode = response.IsSuccessStatusCode.ToString();
                    log.description = response.ReasonPhrase;
                    return log;
                    //string log = WMS.ToXmlUsingDataContract(SO);
                    //WMS.SaveWMSLog("ORDR", "FAILED", SO.DocNumber, "", "", log, response.IsSuccessStatusCode.ToString());
                    //Helper.WriteErrorLog(SO.DocNumber + "-" + response.IsSuccessStatusCode.ToString());
                }

            }

        }
    }
}
