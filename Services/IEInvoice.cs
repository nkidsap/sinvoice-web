﻿using SINVOICE_BASE;
using SINVOICE_WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using inv = SINVOICE_WEB.Models.inv;

namespace SINVOICE_WEB.Services
{
    public interface IEInvoice
    {
        //void SetAPISetting(SettingsModel settingapi);
        Task<ResultFile> GetPDFInvoice(string supplierTaxCode, string invoiceNo, string strIssueDate, string exchangeUser);
        Task<ResultFile> PreviewInvoice(invoice inv);
        Task<List<inv>> FindInvoice(string supplierTaxCode, string startDate, string endDate, string invoiceSeri, int rowPerPage, int pageNum, string templateCode, string invoiceNo, bool getAll = false);
        Task<sinvoicelog> PublishInvoice(invoice inv);
        Task<sinvoicelog> CreateDraftInvoice(invoice inv);
        Task<sinvoicelog> AdjustInvoice(invoice inv, string adjustmentInvoiceType="2", bool isIncreaseItem = true);
        Task<sinvoicelog> CancelInvoice(string supplierTaxCode, string invoiceNo, string strIssueDate, string additionalReferenceDesc, string additionalReferenceDate);
        Task<sinvoicelog> ResendMail(string supplierTaxCode,string lstTransactionUuid);
    }
}
