﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SINVOICE_BASE;
using SINVOICE_WEB.Models;
using SINVOICE_WEB.Services;
using inv = SINVOICE_WEB.Models.inv;

namespace SINVOICE_WEB.Controllers
{
    [Route("api")]
    [ApiController]
    [Authorize]
    public class SInvoiceController : ControllerBase
    {
        private readonly ApplicationContext _context;

        private readonly IDapper _dapper;
        private readonly IEInvoice _einvoice;
        public SettingsModel _Settings { get; }
      
        public SInvoiceController(ApplicationContext context, IDapper dapper,IEInvoice einvoice, Microsoft.Extensions.Options.IOptions<SettingsModel> Settings)
        {
            _context = context;
            _dapper = dapper;
            _einvoice = einvoice;
            _Settings = Settings.Value;
            BaseData.SetDBConnection(context.Database.GetDbConnection().ConnectionString);
            string connectstring = context.Database.GetDbConnection().ConnectionString;
           
          
        }
        
        // GET: GetInvoices
        //[HttpGet]

        //public async Task<ActionResult<IEnumerable<SInvoicesModel>>> GetInvoices()
        //{
        //    return await Task.FromResult(_dapper.GetAll<SInvoicesModel>($"Select * from [Dummy]", null, commandType: CommandType.Text));

        //    //return await _context.Invoices.ToListAsync();
        //}

        [HttpGet("GetPDF")]
        public async Task<IActionResult> GetPDF(string DocEntry,TransType transType,string exchangeuser)
        {
            sinvoicelog invlog = await BaseData.GetSInvoiceInfo(DocEntry, transType);
            Thread.Sleep(3000);
            if (invlog == null)
            {
                //_logger.LogError("Chứng từ chưa phát hành hóa đơn điện tử");
                return BadRequest();
            }
            ResultFile rs= await _einvoice.GetPDFInvoice(invlog.supplierTaxCode, 
               invlog.invoiceNo, 
               invlog.issueDate.ToString("yyyyMMdd000000"),
               exchangeuser);

            if (rs != null && rs.fileToBytes != null)
            {
                byte[] bytes = Convert.FromBase64String(rs.fileToBytes);
                string mimeType = "application/pdf";
                return new FileStreamResult(new MemoryStream(bytes), mimeType)
                {
                    FileDownloadName = rs.fileName + ".pdf"

                };

            }
            else
                return BadRequest();

            //return new FileStreamResult(new MemoryStream(bytes), "application/pdf");

        }
        [HttpGet("PreviewDraft")]
        public async Task<IActionResult> PreviewDraft(string DocEntry,TransType transType)
        {
            invoice inv = await BaseData.GetInvoiceInfo(DocEntry, transType);
            //inv.generalInvoiceInfo.invoiceIssuedDate = inv.generalInvoiceInfo.IssuedDate.ToString("yyyy-MM-ddTHH:mm:ss.fff+0700");// "2020-10-08T20:20:00.000+0700"; // UnixTimeHelper.ToUnixTime(inv.generalInvoiceInfo.IssuedDate);
            Thread.Sleep(3000);
            ResultFile rs = await _einvoice.PreviewInvoice(inv);
            if (rs != null && rs.fileToBytes != null)
            {
                byte[] bytes = Convert.FromBase64String(rs.fileToBytes);
                string mimeType = "application/pdf";
                return new FileStreamResult(new MemoryStream(bytes), mimeType)
                {
                    FileDownloadName = rs.fileName + ".pdf"

                };

            }
            else
                return BadRequest();
            //return new FileStreamResult(new MemoryStream(bytes), "application/pdf");

        }
        //[HttpPost("PublishInvoice")]
        //public async Task<ObjectResult> PostInvoices(SInvoicesModel invoicesModel)
        //{
        //    LogModel log = await PublishInvoice(invoicesModel);
        //    log.objectjson = null;
        //    return Ok(log);
           
        //}

        [HttpPost("PublishDocument")]
        public async Task<IActionResult> PublishDocument(SINVOICE_BASE.Document doc)
        {
            invoice inv = await BaseData.GetInvoiceInfo(doc.DocNo, doc.DocType);
            Thread.Sleep(3000);
            inv.generalInvoiceInfo.invoiceIssuedDate = inv.generalInvoiceInfo.IssuedDate.ToString("yyyy-MM-ddTHH:mm:ss.fff+0700");// "2020-10-08T20:20:00.000+0700"; // UnixTimeHelper.ToUnixTime(inv.generalInvoiceInfo.IssuedDate);

            sinvoicelog log = await _einvoice.PublishInvoice(inv);
            Thread.Sleep(3000);
            if (log.errorCode == null)
            {
                log.docentry = doc.DocNo;
                log.templatecode = inv.generalInvoiceInfo.templateCode;
                log.serial = inv.generalInvoiceInfo.invoiceSeries;
                if (doc.DocType == TransType.ARInvoice)
                    log.transtype = "OINV";
                if (doc.DocType == TransType.ARCreditMemo)
                    log.transtype = "ORIN";
                if (doc.DocType == TransType.Transfer)
                    log.transtype = "OWTR";
                //Lay danh sach Invoice da sync
                Thread.Sleep(3000);
                List<inv> rs = await _einvoice.FindInvoice(log.supplierTaxCode,
                    DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd"),
                    DateTime.Now.ToString("yyyy-MM-dd"),
                    inv.generalInvoiceInfo.invoiceSeries,
                    1,
                    1,
                    inv.generalInvoiceInfo.templateCode,
                     log.invoiceNo
                    );

                //var ls = (from x in rs
                //          where x.invoiceNo == log.invoiceNo
                //          select x).FirstOrDefault<inv>();
                Thread.Sleep(3000);
                if (rs != null && log.invoiceNo != null)
                {
                    log.issuedate_unixtime = rs.FirstOrDefault<inv>().issueDate.ToString();
                    log.issueDate = UnixTimeHelper.ToDateTime(rs.FirstOrDefault<inv>().issueDate);
                }
                else
                {
                    log.issuedate_unixtime = null;
                    log.issueDate = DateTime.Now;
                }
                log.transactionUuid = inv.generalInvoiceInfo.transactionUuid;
                log.templatecode = inv.generalInvoiceInfo.templateCode;
                log.serial = inv.generalInvoiceInfo.invoiceSeries;
                log.paymenttype = inv.payments.FirstOrDefault().paymentMethodName;
                await BaseData.SaveSinvoiceLog(log, "publish");

                return Ok(log);
            }
            else
                return BadRequest(log);

        }

        [HttpPost("AdjustDocument")]
        public async Task<IActionResult> AdjustDocument(SINVOICE_BASE.Document doc)
        {

            invoice inv = await BaseData.GetInvoiceInfo(doc.DocNo, TransType.ARCreditMemo);
            Thread.Sleep(3000);
            inv.generalInvoiceInfo.invoiceIssuedDate = inv.generalInvoiceInfo.IssuedDate.ToString("yyyy-MM-ddTHH:mm:ss.fff+0700");// "2020-10-08T20:20:00.000+0700"; // UnixTimeHelper.ToUnixTime(inv.generalInvoiceInfo.IssuedDate);
           
            sinvoicelog log = await _einvoice.AdjustInvoice(inv, "2", true);
            Thread.Sleep(3000);
            if (log.errorCode == null)
            {
                log.docentry = doc.DocNo;
                log.originalinvoiceNo = inv.generalInvoiceInfo.originalInvoiceId;
                log.templatecode = inv.generalInvoiceInfo.templateCode;
                log.serial = inv.generalInvoiceInfo.invoiceSeries;
                log.transtype = "ORIN";
                ////Lay danh sach Invoice da sync
                Thread.Sleep(3000);
                List<inv> rs = await _einvoice.FindInvoice(log.supplierTaxCode,
                    DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd"),
                    DateTime.Now.ToString("yyyy-MM-dd"),
                    inv.generalInvoiceInfo.invoiceSeries,
                    1,
                    1,
                    inv.generalInvoiceInfo.templateCode,
                    log.invoiceNo
                    );

                //var ls = (from x in rs
                //          where x.invoiceNo == log.invoiceNo
                //          select x).FirstOrDefault<inv>();
                Thread.Sleep(3000);
                if (rs != null && log.invoiceNo != null)
                {
                    log.issuedate_unixtime = rs.FirstOrDefault<inv>().issueDate.ToString();
                    log.issueDate = UnixTimeHelper.ToDateTime(rs.FirstOrDefault<inv>().issueDate);
                }
                else
                {
                    log.issuedate_unixtime = null;
                    log.issueDate = DateTime.Now;
                }
                log.transactionUuid = inv.generalInvoiceInfo.transactionUuid;
                log.templatecode = inv.generalInvoiceInfo.templateCode;
                log.serial = inv.generalInvoiceInfo.invoiceSeries;
                log.paymenttype = inv.payments.FirstOrDefault().paymentMethodName;
                await BaseData.SaveSinvoiceLog(log, "publish");
                return Ok(log);
            }
            else
                return BadRequest(new sinvoicelog() { errorCode = "401", description = "Can't publish document!" });
           

        }

        [HttpPost("ResendMail")]
        public async Task<IActionResult> ResendMail(SINVOICE_BASE.Document doc)
        {

            sinvoicelog inv = await BaseData.GetSInvoiceInfo(doc.DocNo, doc.DocType);
            Thread.Sleep(3000);
            if(inv != null)
            {
                sinvoicelog log = await _einvoice.ResendMail(inv.supplierTaxCode, inv.transactionUuid);
                Thread.Sleep(3000);
                if (log.errorCode == null)
                {

                    return Ok(log);
                }
                else
                    return BadRequest(new sinvoicelog() { errorCode = log.errorCode, description = log.description });

            }
            else
                return BadRequest(new sinvoicelog() { errorCode = "404", description = "Document is not publish!" });


        }



        [HttpPost("CancelDocument")]
        public async Task<IActionResult> CancelDocument(SINVOICE_BASE.Document doc)
        {
            sinvoicelog invlog = await BaseData.GetSInvoiceCancelInfo(doc.DocNo, doc.DocType);
            Thread.Sleep(3000);
            AdditionalReference addref = await BaseData.GetSInvoiceAdditionalReference(invlog.invoiceNo);
            Thread.Sleep(3000);
            if (addref.additionalReferenceDesc != "#")
            {
                sinvoicelog log = await _einvoice.CancelInvoice(invlog.supplierTaxCode, invlog.invoiceNo, invlog.issueDate.ToString("yyyyMMdd000000"), addref.additionalReferenceDesc, addref.additionalReferenceDate);
                Thread.Sleep(3000);

                if (log.errorCode == null)
                {
                    invlog.iscanceled = true;

                    invlog.objectjson = log.objectjson;
                    invlog.errorCode = log.errorCode;
                    invlog.description = log.description;
                    invlog.isupdatetax = false;
                    invlog.ispayment = false;

                    await BaseData.SaveSinvoiceLog(invlog, "cancel");
                    return Ok(log);
                }    
                else
                {
                    invlog.iscanceled = false;

                    invlog.objectjson = log.objectjson;
                    invlog.errorCode = log.errorCode;
                    invlog.description = log.description;
                    invlog.isupdatetax = false;
                    invlog.ispayment = false;

                    await BaseData.SaveSinvoiceLog(invlog, "cancel");
                    return BadRequest(log);
                }    
            }
            else
                return BadRequest(new sinvoicelog() { errorCode="401", description= "Document have been canceled before!" });

        }

        //[HttpPost("FindInvoices")]
        //public async Task<ObjectResult> FindInvoices(FindInvoice find)
        //{

        //    FindInvoiceResult ls = await  FindInvoice(find);

        //    return Ok(ls);

        //}
        //[HttpPost("FindStatusUsingInvoices")]
        //public async Task<ObjectResult> FindInvoiceBySeri(FindInvoiceSeri find)
        //{

        //    FindInvoiceSeriResult ls = await FindInvoiceSeri(find);
        //    if (ls.status != null && ls.status != "200")
        //        return BadRequest(ls);
        //    return Ok(ls);

        //}
       
       
      
        //private async Task<FindInvoiceSeriResult> FindInvoiceSeri(FindInvoiceSeri find)
        //{
        //    var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_Settings.Username}:{_Settings.Password}")));
        //    var api_url = @"InvoiceAPI/InvoiceUtilsWS/getProvidesStatusUsingInvoice";
           
          
        //    using (var client = new HttpClient())
        //    {
        //        LogModel log = new LogModel();
        //        client.BaseAddress = new Uri(_Settings.ApiUrl);
        //        // Add an Accept header for JSON format. 
        //        client.Timeout = new TimeSpan(0, 5, 0);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = authValue;

        //        var content = new StringContent(JsonConvert.SerializeObject(find, Formatting.Indented), Encoding.UTF8, "application/json");

        //        //log.objectjson = JsonConvert.SerializeObject(inv, Formatting.Indented);
        //        var response = client.PostAsync(api_url, content).Result;
        //        if (response.IsSuccessStatusCode)
        //        {
        //            dynamic _output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

        //            FindInvoiceSeriResult output = JsonConvert.DeserializeObject<FindInvoiceSeriResult>(response.Content.ReadAsStringAsync().Result);

        //            if (output.errorCode != null)
        //            {
        //                //log.errorCode = output.errorCode;
        //                //log.description = output.description;
        //                return new FindInvoiceSeriResult();
        //            }

        //            else
        //            {
        //                return output;
        //                //FileHelper.CreateFile("pdf", output.fileName, output.fileToBytes);
        //            }

        //        }
        //        else
        //        {
        //            dynamic output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

        //            return new FindInvoiceSeriResult();
        //        }

        //    }
        //}

    }
}
