﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SINVOICE_WEB.Models;
using SINVOICE_WEB.Services;

namespace SINVOICE_WEB.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IDapper _dapper;
        public TokenController(IConfiguration config, IDapper dapper)
        {
            _config = config;
            _dapper = dapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody] LoginModel login)
        {
            if (login == null) return Unauthorized();
            string tokenString = string.Empty;
            bool validUser = Authenticate(login);
           
            if (validUser)
            {
                tokenString = BuildToken();
            }
            else
            {
                return  Unauthorized();
            }
            return Ok( tokenString);
        }

        private string BuildToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtToken:SecretKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["JwtToken:Issuer"],
              _config["JwtToken:Issuer"],
              expires: DateTime.Now.AddDays(1),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private  bool Authenticate(LoginModel login)
        {
            bool validUser = false;
            string valid= (string) _dapper.ExecuteScalar($"SP_CHECK_LOGIN @Username='{login.Username}',@password='{login.Password}'", null, commandType: CommandType.Text);

            if (valid == "1")
             {
                validUser = true;
            }
            return validUser;
        }

       

    }
}
