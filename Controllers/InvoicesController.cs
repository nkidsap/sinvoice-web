﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SINVOICE_WEB.Models;
using SINVOICE_WEB.Services;

namespace SINVOICE_WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class InvoiceController : ControllerBase
    {
        private readonly ApplicationContext _context;

        private readonly IDapper _dapper;
        public MySettingsModel _MySettings { get; }

        public InvoiceController(ApplicationContext context, IDapper dapper, Microsoft.Extensions.Options.IOptions<MySettingsModel> MySettings)
        {
            _context = context;
            _dapper = dapper;
            _MySettings = MySettings.Value;
            string connectstring = context.Database.GetDbConnection().ConnectionString;
        }

        // GET: GetInvoices
        [HttpGet]

        public async Task<ActionResult<IEnumerable<InvoicesModel>>> GetInvoices()
        {
            return await Task.FromResult(_dapper.GetAll<InvoicesModel>($"Select * from [Dummy]", null, commandType: CommandType.Text));

            //return await _context.Invoices.ToListAsync();
        }

        //// GET: api/InvoicesAPI/5
        //[HttpGet("{id}")]
        //public async Task<InvoicesModel> GetInvoicesModel(string id)
        //{
        //    var invoicesModel = await Task.FromResult(_dapper.GetByKey<InvoicesModel>($"Select * from [Dummy] where Id = {id}", null, commandType: CommandType.Text));
        //    //return result;
        //    //var invoicesModel = await _context.Invoices.FindAsync(id);

        //    if (invoicesModel == null)
        //    {
        //        //return NotFound();
        //        return null;
        //    }

        //    return invoicesModel;
        //}

        //// PUT: api/InvoicesAPI/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public   Task<int> PutInvoicesModel(string id, InvoicesModel invoicesModel)
        //{
        //    if (id != invoicesModel.DocEntry)
        //    {
        //        //return BadRequest();
        //        return null;
        //    }
        //    var dbPara = new DynamicParameters();
        //    dbPara.Add("Id", invoicesModel.DocEntry);


        //    var updateArticle = Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_Article]",
        //                    dbPara,
        //                    commandType: CommandType.StoredProcedure));
        //    //_context.Entry(invoicesModel).State = EntityState.Modified;

        //    //try
        //    //{
        //    //    await _context.SaveChangesAsync();
        //    //}
        //    //catch (DbUpdateConcurrencyException)
        //    //{
        //    //    if (!InvoicesModelExists(id))
        //    //    {
        //    //        return NotFound();
        //    //    }
        //    //    else
        //    //    {
        //    //        throw;
        //    //    }
        //    //}

        //    //return NoContent();
        //    return updateArticle;
        //}

        // POST: api/InvoicesAPI
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<int> PostInvoicesModel(InvoicesModel invoicesModel)
        {
            var dbparams = new DynamicParameters();
            //dbparams.Add("Id", invoicesModel.DocEntry, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_Article]"
                , dbparams,
                commandType: CommandType.StoredProcedure));


            //_context.Invoices.Add(invoicesModel);
            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateException)
            //{
            //    if (InvoicesModelExists(invoicesModel.DocEntry))
            //    {
            //        return Conflict();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            //return CreatedAtAction("GetInvoicesModel", new { id = invoicesModel.DocEntry }, invoicesModel);
            return result;
        }

        //// DELETE: api/InvoicesAPI/5
        //[HttpDelete("{id}")]
        //public async Task<int> DeleteInvoicesModel(string id)
        //{
        //    //var invoicesModel = await _context.Invoices.FindAsync(id);
        //    var invoicesModel = await GetInvoicesModel(id);
        //    if (invoicesModel == null)
        //    {
        //        //return NotFound();
        //        return -1;
        //    }
        //    var result = await Task.FromResult(_dapper.Execute($"Delete [Dummy] Where Id = {id}", null, commandType: CommandType.Text));
        //    //_context.Invoices.Remove(invoicesModel);
        //    //await _context.SaveChangesAsync();

        //    //return invoicesModel;
        //    return result;
        //}

        //private bool InvoicesModelExists(string id)
        //{
        //    return _context.Invoices.Any(e => e.DocEntry == id);
        //}
        public async Task<invoice> GetInvoiceInfo(string DocEntry, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_INVOICE_INFO",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();
                if (sum.discountAmount != 0)
                {
                    its.Add(new itemInfo() { lineNumber = its.Count + 1, selection = 3, itemCode = "chiet_khau_hang_hoa", itemName = "Chiết khấu hàng hóa", taxAmount = 0, discount = 0, taxPercentage = 0, itemDiscount = 0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                }
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

    }
}
