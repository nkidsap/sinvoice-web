﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SINVOICE_WEB.Models;
using SINVOICE_WEB.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SINVOICE_WEB.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class MailerController : ControllerBase
    {


        private readonly ILogger<MailerController> _logger;
        private readonly IMailer _mailer;

        public MailerController(ILogger<MailerController> logger, IMailer mailer)
        {
            _logger = logger;
            _mailer = mailer;
        }

        [HttpPost]
        [Route("Send")]
        public async Task<IActionResult> Send(Mail m)
        {

            await _mailer.SendEmailAsync(m);

            return Ok();
        }
    }
}
