﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;

namespace SINVOICE_WEB.Controllers
{
    public class EInvoiceController : Controller
    {
        List<SINVOICE_WEB.Models.Document> lsDocs= new List<SINVOICE_WEB.Models.Document>();
        // GET: E_InvoiceController
        public ActionResult Index()
        {
            lsDocs = new List<SINVOICE_WEB.Models.Document>()
            { 
                new SINVOICE_WEB.Models.Document(){ Docentry=1, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0001", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=2, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0002", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=3, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0003", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=4, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0004", IssueDate = DateTime.Now}
            };
            return View(lsDocs);
        }

        // GET: E_InvoiceController/Details/5
        public ActionResult Details(int id)
        {
            
            return View(GetDocById(id));
        }

        // GET: E_InvoiceController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: E_InvoiceController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: E_InvoiceController/Edit/5
        public ActionResult Edit(int id)
        {
           
            return View(GetDocById(id));
        }

        // POST: E_InvoiceController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: E_InvoiceController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: E_InvoiceController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public SINVOICE_WEB.Models.Document GetDocById(int id)
        {
            lsDocs = new List<SINVOICE_WEB.Models.Document>()
            {
                new SINVOICE_WEB.Models.Document(){ Docentry=1, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0001", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=2, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0002", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=3, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0003", IssueDate = DateTime.Now},
                new SINVOICE_WEB.Models.Document(){ Docentry=4, TemplateCode="01GTGT-01",Serial="AA/20P", InvoiceNo="AA/20P0004", IssueDate = DateTime.Now}
            };
            return lsDocs.Where(x => x.Docentry == id).FirstOrDefault();
           
        }
    }
}
